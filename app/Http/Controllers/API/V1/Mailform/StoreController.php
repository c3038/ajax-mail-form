<?php

namespace App\Http\Controllers\API\V1\Mailform;

use App\Http\Requests\V1\Mailform\StoreRequest;
use App\Http\Resources\V1\Mailform\MailformResource;
use App\Models\Feedback;
use Illuminate\Http\Request;

class StoreController extends BaseController
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request->validated();
        $page = $this->service->store($data);

        if (!($page instanceof Feedback)) {
            return $this->sendError($page);
        }
        return $this->sendResponse(new MailformResource($page), 'Data created.');
    }
}
