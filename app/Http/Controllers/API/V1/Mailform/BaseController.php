<?php

namespace App\Http\Controllers\API\V1\Mailform;

use App\Http\Controllers\Controller;
use App\Service\V1\MailformService;

class BaseController extends Controller
{
   public $service;

   public function __construct(MailformService $service)
   {
      $this->service = $service;
   }

   public function sendResponse($result, $message)
   {
      $response = [
         'success' => true,
         'data'    => $result,
         'message' => $message,
      ];
      return response()->json($response, 200);
   }

   public function sendError($error, $errorMessages = [], $code = 404)
   {
      $response = [
         'success' => false,
         'message' => $error,
      ];
      if (!empty($errorMessages)) {
         $response['data'] = $errorMessages;
      }
      return response()->json($response, $code);
   }
}
