<?php

namespace App\Http\Requests\V1\Mailform;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {

        return [
            'name' => 'required|string',
            'phone' => 'required|string',
            'email' => 'nullable|email',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Это поле необходимо для заполнения',
            'name.string' => 'Данные должны соответствовать строчному типу',
            'phone.required' => 'Это поле необходимо для заполнения',
            'phone.string' => 'Данные должны соответствовать строчному типу',
            'email.email' => ' Поле должно соответствовать формату email@example.domain',
        ];
    }
}
