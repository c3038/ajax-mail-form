<?php

namespace App\Service\V1;

use App\Http\Resources\Mailform\MailformResource;
use App\Mail\Mailform\FormData;
use App\Models\Feedback;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class MailformService
{
   public function store(array $data)
   {
      try {
         DB::beginTransaction();
         $page =  Feedback::create($data);
         DB::commit();
      } catch (\Exception $exception) {
         DB::rollBack();
         return $exception->getMessage();
      }

      if (isset($data['email'])) {
         Mail::to($data['email'])->send(new FormData($data));
      }

      return $page;
   }
}
