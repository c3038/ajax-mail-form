@extends('mailform.layouts.main')

@section('content')

<!-- name, phone, email -->
<form name="emailform" id="emailform" name="emailform" class="col-6">
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Имя</label>
        <input type="text" class="form-control" id="name" name="name" required>
        <div class="alert alert-danger mt-2 d-none" id="name-error" role="alert"></div>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Телефон</label>
        <input type="tel" class="form-control" id="phone" name="phone" required>
        <div class="alert alert-danger mt-2 d-none" id="phone-error" role="alert"></div>
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Email</label>
        <input type="email" class="form-control" id="email" name="email">
        <div class="alert alert-danger mt-2 d-none" id="email-error" role="alert"></div>
    </div>
    <button type="button" id="btn-save" class="btn btn-primary">Отправить</button>
</form>

@include('mailform.modal.mail_form_modal')

@endsection