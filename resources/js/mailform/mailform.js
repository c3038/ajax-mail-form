$(window).on("load", function() {

   $('#btn-save').on('click', function(e) {
      e.preventDefault();

      const name = $('#name').val(),
         phone = $('#phone').val(),
         email = $('#email').val();

      $('#name-error').addClass('d-none')
      $('#phone-error').addClass('d-none')

      $.ajaxSetup({
         headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
               'Accept': 'application/json'
         }
      });

      $.ajax({
         url: "/api/v1/mailform",
         type: "post",
         dataType: "json",
         data: {
               "name": name,
               "phone": phone,
               "email": email
         },
         error(err) {
               let errorData = err.responseJSON.errors
               for (let er in errorData) {
                  const errorText = errorData[er][0]
                  $(`#${er}-error`).removeClass('d-none').text(errorText)
               }
         },
         success(data) {
            $('#name').val('');
            $('#phone').val('');
            $('#email').val('');
            const modal = new bootstrap.Modal(document.querySelector('.modal'));
            modal.show();
         }
      })
   })
})